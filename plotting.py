from datetime import timedelta
from typing import List

from jira_exporter import Backlog, Status, to_days
import matplotlib.pyplot as plt
import numpy as np

WIP_STATUSES = (Status.IN_PROGRESS, Status.IN_REVIEW)
PERCENTILES = (25, 50, 85, 95)

PERCENTILE_COLOR = 'red'
COLORS = ('purple', 'orange', 'brown', 'blue', 'gray')


def _cycle_times_in_days(issue_cycle_times: List[timedelta]):
    return list(map(lambda delta: to_days(delta) + 1, issue_cycle_times))


def plot_cycle_time_scatter_plot(backlog: Backlog, *, percentiles=PERCENTILES):
    issue_cycle_times = backlog.cycle_times()
    done_dates = list(map(lambda issue: issue.done_at(), issue_cycle_times.keys()))
    cycle_times = _cycle_times_in_days(list(issue_cycle_times.values()))
    percentile_values = [round(p, 2) for p in np.percentile(cycle_times, percentiles)]

    # General plot settings
    fig, ax1 = plt.subplots()
    ax1.set_title('Cycle time scatterplot')

    # Subplot 1
    ax1.scatter(done_dates, cycle_times)
    ax1.set_xlabel('Done Date')
    ax1.set_ylabel('Number of days')

    # Subplot 2
    ax2 = ax1.twinx()
    for perc in percentile_values:
        ax2.axhline(perc, color=PERCENTILE_COLOR)
    ax2.set_yticks(percentile_values, percentile_values)
    ax2.set_ylim(ax1.get_ylim())

    plt.show()


def plot_cycle_time_histogram(backlog: Backlog, *, percentiles=PERCENTILES):
    cycle_times = _cycle_times_in_days(list(backlog.cycle_times().values()))
    percentile_values = [round(p, 2) for p in np.percentile(cycle_times, percentiles)]

    # General plot settings
    fig, ax1 = plt.subplots()
    ax1.set_title('Cycle time histogram')

    # Subplot 1
    ax1.hist(x=cycle_times)
    ax1.set_xlabel('Cycle time')
    ax1.set_ylabel('Frequency')

    # Subplot 2
    ax2 = ax1.twiny()
    for perc in percentile_values:
        ax2.axvline(perc, color=PERCENTILE_COLOR)
    ax2.set_xticks(percentile_values, percentile_values)
    ax2.set_xlim(ax1.get_xlim())

    plt.show()


def _filter_by_status(work_item_ages, status):
    return {item: to_days(age) for item, age in work_item_ages.items() if item.status == status}


def plot_work_item_age_chart(backlog, *, statuses=WIP_STATUSES, percentiles=PERCENTILES, colors=COLORS):
    all_work_item_ages = backlog.work_item_ages()

    # General plot settings
    fig, ax = plt.subplots()
    ax.set_title('Work Item Age Chart')
    ax.set_xlabel('Status')
    ax.set_ylabel('Age')

    # Scatter plots
    for i, status in enumerate(statuses):
        work_item_ages_dict = _filter_by_status(all_work_item_ages, status).items()
        ages = [entry[1] for entry in work_item_ages_dict]

        ax.scatter(ages, ages, color=colors[i])

    ax.set_xticks(range(len(statuses)))
    ax.set_xticklabels(statuses)

    # Percentiles
    cycle_times = _cycle_times_in_days(list(backlog.cycle_times().values()))
    percentile_values = [round(p, 2) for p in np.percentile(cycle_times, percentiles)]

    ax2 = ax.twinx()
    for perc in percentile_values:
        ax2.axhline(perc, color=PERCENTILE_COLOR)
    ax2.set_yticks(percentile_values, percentile_values)

    max_y = max(ax.get_ylim()[1], ax2.get_ylim()[1])
    ax.set_ylim([0, max_y])
    ax2.set_ylim([0, max_y])

    plt.show()


def plot_throughput_per_day(backlog):
    throughput_per_day = backlog.throughput_per_day()
    dates = throughput_per_day.keys()
    xs = range(len(dates))
    plt.bar(xs, throughput_per_day.values())
    plt.xticks(ticks=xs, labels=dates, rotation=90)

    plt.title('Throughput per day')
    plt.xlabel('Date')
    plt.ylabel('Throughput')

    plt.show()


def plot_mcs_results(simulation_results, *, percentiles=PERCENTILES, bins=50):
    percentile_values = [round(p, 2) for p in np.percentile(simulation_results, percentiles)]

    # General plot settings
    fig, ax1 = plt.subplots()
    ax1.set_title('Monte Carlo Simulation')

    # Subplot 1
    ax1.hist(x=simulation_results, bins=bins)
    ax1.set_xlabel('Days to finish')
    ax1.set_ylabel('Frequency')

    # Subplot 2
    ax2 = ax1.twiny()
    for perc in percentile_values:
        ax2.axvline(perc, color=PERCENTILE_COLOR)
    ax2.set_xticks(percentile_values, percentile_values)
    ax2.set_xlim(ax1.get_xlim())

    plt.show()
