# Setup

- Create a personal Atlassian API token: https://id.atlassian.com/manage-profile/security/api-tokens
- Setup a virtual env: `python3 -mvenv venv`
- `. ./venv/bin/activate`
- `pip install -r requirements.txt`

# Configuration

- Copy `default.env` to `.env`
- Open `.env` with a text editor and set all the appropriate values
  
# Exporting CSV with matching jira tickets

- `. ./venv/bin/activate`
- Make sure that the `JIRA_QUERY` variable in your `.env` is set to a valid jira query
- Run `./jira-exporter.py > export.csv`

# Running the jupiter notebook

- `. ./venv/bin/activate`
- `./scripts/run-notebook`

To clear the output of a jupyter notebook (so it can be commited cleanly):

```
./scripts/clear-output
```
