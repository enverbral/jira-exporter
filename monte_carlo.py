from typing import Callable, Protocol

from jira_exporter import Backlog
import random


class Simulation(Protocol):
    def has_ended(self) -> bool:
        ...

    def tick(self):
        ...

    def result(self) -> int:
        ...


class NumberOfDaysToFinishStories(Simulation):
    def __init__(self, backlog: Backlog, stories_to_finish: int):
        self.throughput_per_day = backlog.throughput_per_day()
        self.stories_to_finish = stories_to_finish
        self.finished_stories = []

    def has_ended(self) -> bool:
        return sum(self.finished_stories) >= self.stories_to_finish

    def tick(self):
        self.finished_stories.append(self.simulate_throughput())

    def simulate_throughput(self) -> int:
        return random.choice(list(self.throughput_per_day.values()))

    def result(self) -> int:
        return len(self.finished_stories)


def run_single_simulation(simulation: Simulation) -> int:
    while not simulation.has_ended():
        simulation.tick()
    return simulation.result()


def run_monte_carlo_simulation(create_simulation: Callable[[], Simulation], *, runs=10_000):
    all_results = []
    for run in range(runs):
        simulation = create_simulation()
        result = run_single_simulation(simulation)
        all_results.append(result)
    return all_results
