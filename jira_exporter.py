#!/usr/bin/env python3
from collections import Counter
from typing import Iterable, Dict

import string
import itertools
import numpy as np
from datetime import datetime, timedelta, date
from pytz import timezone
from enum import Enum
from typing import Optional
from dateutil import parser
from environs import Env

from jira import JIRA

ATLASSIAN_URL_ENVIRONMENT_VARIABLE = 'ATLASSIAN_URL'
ATLASSIAN_USERNAME_ENVIRONMENT_VARIABLE = 'ATLASSIAN_USERNAME'
ATLASSIAN_TOKEN_ENVIRONMENT_VARIABLE = 'ATLASSIAN_TOKEN'
TIMEZONE_ENVIRONMENT_VARIABLE = 'TIMEZONE'
JIRA_QUERY_ENVIRONMENT_VARIABLE = 'JIRA_QUERY'
STORY_POINTS_FIELD_ENVIRONMENT_VARIABLE = 'STORY_POINTS_FIELD'

env = Env()
env.read_env()

TIMEZONE = timezone(env.str(TIMEZONE_ENVIRONMENT_VARIABLE))
JIRA_QUERY = env.str(JIRA_QUERY_ENVIRONMENT_VARIABLE)
STORY_POINTS_FIELD = env.str(STORY_POINTS_FIELD_ENVIRONMENT_VARIABLE)

find = lambda f, xs: next(filter(f, xs), None)
last = lambda xs: xs[-1] if len(xs) > 0 else None

STATUSES = [
    'Todo',
    'In Progress',
    'In Review',
    'Done'
]


def date_range(earliest_day, latest_day_inclusive):
    dates = []
    current_date = earliest_day
    while current_date <= latest_day_inclusive:
        dates.append(current_date)
        current_date = current_date + timedelta(days=1)
    return dates


def end_of_day(date):
    return datetime(date.year, date.month, date.day, 23, 59, 59).replace(tzinfo=TIMEZONE)


def to_hours(delta: timedelta) -> float:
    return delta.total_seconds() / 3600


def to_days(delta: timedelta) -> float:
    return to_hours(delta) / 24


class TimeRange:
    def __init__(self, start: datetime, end: datetime):
        self.start = start
        self.end = end

    def duration(self) -> timedelta:
        return self.end - self.start


class Status(Enum):
    TODO = 'Todo'
    IN_PROGRESS = 'In Progress'
    IN_REVIEW = 'In Review'
    DONE = 'Done'

    @staticmethod
    def parse(status: string):
        return find(lambda x: x.value == status, map(lambda x: x[1], Status.__members__.items()))


class IssueHistoryEntry:
    def __init__(self, status: Status, author: string, created: datetime):
        self.status = status
        self.author = author
        self.created = created

    def __str__(self):
        return "{} {} {}".format(self.status, self.author, self.created)


class IssueHistory:
    def __init__(self, entries: list[IssueHistoryEntry]):
        self.entries = entries

    def time_spent_in(self, status: Status) -> timedelta:
        since = None
        latest_transition_time = self._latest_transition_time()

        total_time_spent = timedelta()

        while since is None or since < latest_transition_time:
            time_range = self._time_range_spent_in_since(status, since)
            if time_range is None:
                break

            total_time_spent += time_range.duration()
            since = time_range.end

        return total_time_spent

    def is_created_at(self, time: datetime) -> bool:
        return self._is_in_status_at(Status.TODO, time)

    def is_done_at(self, time: datetime) -> bool:
        return self._is_in_status_at(Status.DONE, time)

    def done_at(self):
        return self.last_entered_status_at(Status.DONE)

    def first_entered_status_at(self, status: Status) -> Optional[datetime]:
        return self._entered_status_at(status)

    def last_entered_status_at(self, status: Status) -> Optional[datetime]:
        last_entered_status_at = None
        entered_status_at = self._entered_status_at(status)

        while entered_status_at:
            last_entered_status_at = entered_status_at
            entered_status_at = self._entered_status_at(status, since=entered_status_at)

        return last_entered_status_at

    def _is_in_status_at(self, status: Status, time: datetime) -> bool:
        entered_status_at = self._entered_status_at(status)
        return entered_status_at is not None and entered_status_at <= time

    def _time_range_spent_in_since(self, status, since) -> Optional[TimeRange]:
        entered_status_at = self._entered_status_at(status, since=since)
        if entered_status_at is None:
            return None

        left_status_at = self._left_status_at(status, since=entered_status_at)
        if left_status_at is None:
            left_status_at = datetime.now().replace(tzinfo=TIMEZONE)

        return TimeRange(entered_status_at, left_status_at)

    def _entered_status_at(self, status: Status, *, since: Optional[datetime] = None) -> Optional[datetime]:
        entered_status_entry = find(lambda entry: entry.status == status, self._entries_since(since))
        if entered_status_entry is None:
            return None

        return entered_status_entry.created

    def _left_status_at(self, status: Status, *, since: Optional[datetime] = None) -> Optional[datetime]:
        entered_status_entry = find(lambda entry: entry.status != status, self._entries_since(since))
        if entered_status_entry is None:
            return None

        return entered_status_entry.created

    def _entries_since(self, since: Optional[datetime] = None):
        return filter(lambda entry: entry.created > since, self.entries) if since is not None else self.entries

    def _latest_transition_time(self):
        last_entry = last(self.entries)
        return last_entry.created if last_entry else None


class Issue:
    def __init__(self, key: string, status: Status, story_points: int, history: IssueHistory):
        self.key = key
        self.status = status
        self.story_points = story_points
        self.history = history

    def time_spent_in(self, status: Status):
        return self.history.time_spent_in(status)

    def is_created_at(self, time: datetime):
        return self.history.is_created_at(time)

    def is_done_at(self, time: datetime):
        return self.history.is_done_at(time)

    def done_at(self):
        return self.history.done_at()

    def cycle_time(self) -> Optional[timedelta]:
        entered_system_at = self.started_work_at()
        if not entered_system_at:
            return None

        left_system_at = self.finished_work_at()
        if not left_system_at:
            return None

        return left_system_at - entered_system_at

    def age(self) -> Optional[timedelta]:
        if self.status == Status.DONE:
            return None

        entered_system_at = self.started_work_at()
        if not entered_system_at:
            return None

        return datetime.now().replace(tzinfo=TIMEZONE) - entered_system_at

    def started_work_at(self) -> Optional[datetime]:
        return self.history.first_entered_status_at(Status.IN_PROGRESS)

    def finished_work_at(self) -> Optional[datetime]:
        return self.history.last_entered_status_at(Status.DONE)

    def __repr__(self):
        return f"Issue {self.key}"


def count_story_points(items: Iterable[Issue]):
    return sum(map(lambda issue: issue.story_points or 0, items))


def count_items(items: Iterable[Issue]):
    return sum([1 for _ in items])


class Backlog:
    def __init__(self, items: list[Issue]):
        self.items = items

    def story_points_created_at(self, time: datetime):
        return count_story_points(self.items_created_at(time))

    def count_items_created_at(self, time: datetime):
        return count_items(self.items_created_at(time))

    def items_created_at(self, time: datetime) -> Iterable[Issue]:
        return filter(lambda issue: issue.is_created_at(time), self.items)

    def story_points_done_at(self, time: datetime):
        return count_story_points(self.items_done_at(time))

    def count_items_done_at(self, time: datetime):
        return count_items(self.items_done_at(time))

    def items_done_at(self, time: datetime) -> Iterable[Issue]:
        return filter(lambda issue: issue.is_done_at(time), self.items)

    def cycle_times(self) -> Dict[Issue, timedelta]:
        return {issue: issue.cycle_time() for issue in self.items if issue.cycle_time()}

    def work_item_ages(self) -> Dict[Issue, timedelta]:
        return {issue: issue.age() for issue in self.items if issue.age()}

    def throughput_per_day(self) -> Dict[date, int]:
        throughput_per_day = Counter([item.finished_work_at().date() for item in self.items])

        earliest_day = min(throughput_per_day.keys())
        latest_day = max(throughput_per_day.keys())

        return {day: throughput_per_day[day] or 0 for day in date_range(earliest_day, latest_day)}


class BacklogProjection:
    def __init__(self, date_range, points_created, points_done):
        self.date_range = date_range
        self.points_created = points_created
        self.points_done = points_done

    def average_completion_rate(self):
        return self._average_diff_of(self.points_done)

    def average_backlog_growth(self):
        return self._average_diff_of(self.points_created)

    def _average_diff_of(self, points: list[int]):
        point_pairs = zip(points[1:], points[:-1])
        point_diffs = list(map(lambda pair: pair[0] - pair[1], point_pairs))
        return np.mean(point_diffs)

    def projected_end_date(self) -> Optional[datetime]:
        projected_end_delta = self._projected_end_in_days_relative_to_start()
        return self.date_range[0] + timedelta(days=projected_end_delta) if projected_end_delta else None

    def _projected_end_in_days_relative_to_start(self) -> Optional[int]:
        # calculate intersection of average progress line and average backlog growth line
        denominator = self.average_completion_rate() - self.average_backlog_growth()

        if -0.00001 <= denominator <= 0.00001:
            return None

        return round((self.points_created[0] - self.points_done[0]) / denominator)


def project_by_item_count(backlog, date_range) -> BacklogProjection:
    points_created = [backlog.count_items_created_at(end_of_day(day)) for day in date_range]
    points_done = [backlog.count_items_done_at(end_of_day(day)) for day in date_range]

    return BacklogProjection(date_range, points_created, points_done)


def project_by_story_points(backlog, date_range) -> BacklogProjection:
    points_created = [backlog.story_points_created_at(end_of_day(day)) for day in date_range]
    points_done = [backlog.story_points_done_at(end_of_day(day)) for day in date_range]

    return BacklogProjection(date_range, points_created, points_done)


class JiraClient:
    def __init__(self, atlassian_url: string, atlassian_username: string, atlassian_token: string):
        self.jira = JIRA(atlassian_url, basic_auth=(atlassian_username, atlassian_token))

    def get_issues(self, query, *, limit=False) -> list[Issue]:
        issues = self._fetch_jira_issues(query, limit=limit)
        return list(map(self._parse_issue, issues))

    def _fetch_jira_issues(self, query, *, limit):
        return self.jira.search_issues(query, maxResults=limit, expand='changelog')

    def _parse_issue(self, issue):
        status = Status.parse(str(issue.fields.status))
        story_points = getattr(issue.fields, STORY_POINTS_FIELD)
        history = self._parse_issue_history(issue)
        return Issue(issue.key, status, story_points, history)

    def _parse_issue_history(self, issue) -> IssueHistory:
        entries = self._parse_history_entries(issue)
        return IssueHistory(entries)

    def _parse_history_entries(self, issue) -> list[IssueHistoryEntry]:
        initial_entry = self._parse_initial_entry(issue)
        other_entries = list(filter(lambda x: x is not None, map(self._parse_history_entry, issue.changelog.histories)))
        all_entries = [initial_entry] + other_entries

        return sorted(all_entries, key=lambda entry: entry.created)

    def _parse_history_entry(self, history) -> Optional[IssueHistoryEntry]:
        status_item = find(lambda item: item.field == 'status', history.items)
        if not status_item:
            return None

        to_status = Status.parse(status_item.toString)
        created = parser.parse(history.created)

        author = None
        if hasattr(history, 'author'):
            author = history.author

        return IssueHistoryEntry(to_status, author, created)

    def _parse_initial_entry(self, issue):
        return IssueHistoryEntry(Status.TODO, issue.fields.creator, parser.parse(issue.fields.created))


def print_csv_row(row: list[string]):
    print(",".join(map(lambda x: "\"{}\"".format(x), row)))


def create_jira_client():
    atlassian_url = env.str(ATLASSIAN_URL_ENVIRONMENT_VARIABLE)
    atlassian_username = env.str(ATLASSIAN_USERNAME_ENVIRONMENT_VARIABLE)
    atlassian_token = env.str(ATLASSIAN_TOKEN_ENVIRONMENT_VARIABLE)

    return JiraClient(atlassian_url, atlassian_username, atlassian_token)


def main():
    jira = create_jira_client()

    issues = jira.get_issues(JIRA_QUERY)

    print_csv_row([
        "key",
        "status",
        "story_points",
        "time_in_todo",
        "time_in_progress",
        "time_in_review",
        "time_in_done",
    ])

    for issue in issues:
        print_csv_row([
            issue.key,
            issue.status.value,
            issue.story_points,
            issue.time_spent_in(Status.TODO).total_seconds(),
            issue.time_spent_in(Status.IN_PROGRESS).total_seconds(),
            issue.time_spent_in(Status.IN_REVIEW).total_seconds(),
            issue.time_spent_in(Status.DONE).total_seconds()
        ])


if __name__ == "__main__":
    main()
